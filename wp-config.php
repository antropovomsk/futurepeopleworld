<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'futuredb');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|e9}X**>avO{9[[YeGd3W5fbjtKIEy8Vo~*J&L?e=eU)$eVmzOBj<ccCjNrCt2i^');
define('SECURE_AUTH_KEY',  'bL])f^>5WV%<&k|NLqjJa!]5E5B>T6/wdT|4246<4]_h)7@HE~qF ;G [/Vp0EZJ');
define('LOGGED_IN_KEY',    'SXUCe@hhy:b;sO(iNN=$xI)!SbIvsel-;iagg@$$7Cmr39Xv8RW:gaD{px]jIiVn');
define('NONCE_KEY',        'uC>]d{v/vm{ArL<`)tz1:-l9tCoA4rX7^59)=n2m0&7~<~r1[y3S{cNlDr1nRO^c');
define('AUTH_SALT',        '7NPR]MO~G%S4<]kkd4,v=6R&{4v@r ehW;au&gLbZukYZW7F.T6AZ{wf]fCO->Ag');
define('SECURE_AUTH_SALT', 'AF[od -Z^{uP=Ib=%NR]w!w?/7o0)n8Y tL6ZE4_d%}*^&;?i|03Z`}v8v`aI|iw');
define('LOGGED_IN_SALT',   'g%y&QqUhIoJ6fY[[,Fiqz%8~t4G.h6]F%LyKc1r?Z[3?9|WJuflXKq-4lAft)L~8');
define('NONCE_SALT',       '<Nu|%%QWn~~/xDjy_RnK/,y`7f2l#s1AhP$Gx3hK#AEs0q{zwb8P<4pOj|E{ 3fr');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'fh5_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
